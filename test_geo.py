"""Unit test for the geo module"""

from floodsystem.geo import stations_within_radius, stations_by_distance, rivers_with_station, stations_by_river, rivers_by_station_number
from floodsystem.stationdata import MonitoringStation

def test_stations_within_radius():
    """Test creating list of stations within radius r of coordinate x."""
    # Create a couple of test stations.
    stations = [MonitoringStation("id1", "measure1", "station1", (0, 0), (0, 1), "river1", "town1"),
                MonitoringStation("id2", "measure2", "station2", (1, 1), (0, 1), "river2", "town2")]

    stations_within_radius_list = stations_within_radius(stations, (0, 0), 1)
    assert len(stations_within_radius_list) > 0
    assert stations_within_radius_list == [(stations[0].name, 0)]

test_stations_within_radius()


def test_stations_by_distance():
    """Test creating list of stations sorted by distance to Cambridge."""
    # Create a couple of test stations.
    stations = [MonitoringStation("id1", "measure1", "station1", (10, 50), (0, 1), "river1", "town1"),
				MonitoringStation("id2", "measure2", "station2", (10, 1), (0, 1), "river2", "town2"),
				MonitoringStation("id2", "measure2", "station2", (0.1, 0), (0, 1), "river2", "town2"),
				MonitoringStation("id2", "measure2", "station2", (100, 100), (0, 1), "river2", "town2")]

    stations_by_distance_list = stations_by_distance(stations, (0, 0))
    assert len(stations_by_distance_list) > 0
    assert stations_by_distance_list[0][0] == stations[2]
    assert stations_by_distance_list[-1][0] == stations[3]

test_stations_by_distance()

def test_rivers_with_station():
	"""Test creating list of n rivers with the largest number of stations."""
	# Create a few test stations.
	stations = [MonitoringStation("id1", "measure1", "station1", (0, 0), (0, 1), "river1", "town1"),
				MonitoringStation("id2", "measure2", "station2", (1, 1), (0, 1), "river3", "town2"),
				MonitoringStation("id3", "measure1", "station3", (0, 1), (0, 1), "river2", "town2"),
				MonitoringStation("id4", "measure1", "station4", (1, 0), (0, 1), "river3", "town3"),
				MonitoringStation("id5", "measure1", "station5", (0.5, 0.5), (0, 1), "river3", "town3"),
				MonitoringStation("id6", "measure1", "station6", (1.5, 1.5), (0, 1), "river2", "town3")]
	rivers = rivers_with_station(stations)
	assert len(rivers) > 0
	assert rivers == {'river1', 'river3', 'river2'}

test_rivers_with_station()

def test_stations_by_river():
	"""Test creating list of n rivers with the largest number of stations."""
	# Create a few test stations.
	stations = [MonitoringStation("id1", "measure1", "station1", (0, 0), (0, 1), "river1", "town1"),
				MonitoringStation("id2", "measure2", "station2", (1, 1), (0, 1), "river3", "town2"),
				MonitoringStation("id3", "measure1", "station3", (0, 1), (0, 1), "river2", "town2"),
				MonitoringStation("id4", "measure1", "station4", (1, 0), (0, 1), "river3", "town3"),
				MonitoringStation("id5", "measure1", "station5", (0.5, 0.5), (0, 1), "river3", "town3"),
				MonitoringStation("id6", "measure1", "station6", (1.5, 1.5), (0, 1), "river2", "town3")]
	rivers = stations_by_river(stations)
	stations_on_river3 = [station.name for station in rivers['river3']]
	assert len(rivers) > 0
	assert stations_on_river3 == ['station2', 'station4', 'station5']

test_stations_by_river()

def test_rivers_by_station_number():
    """Test creating list of n rivers with the largest number of stations."""
    # Create a few test stations.
    stations = [MonitoringStation("id1", "measure1", "station1", (0, 0), (0, 1), "river1", "town1"),
                MonitoringStation("id2", "measure2", "station2", (1, 1), (0, 1), "river2", "town2"),
                MonitoringStation("id3", "measure1", "station3", (0, 1), (0, 1), "river2", "town2"),
                MonitoringStation("id4", "measure1", "station4", (1, 0), (0, 1), "river3", "town3"),
                MonitoringStation("id5", "measure1", "station5", (0.5, 0.5), (0, 1), "river3", "town3"),
                MonitoringStation("id6", "measure1", "station6", (1.5, 1.5), (0, 1), "river3", "town3")]
    rivers_with_most_stations = rivers_by_station_number(stations, 3)
    assert len(rivers_with_most_stations) > 0
    assert rivers_with_most_stations == [('river3', 3), ('river2', 2), ('river1', 1)]

test_rivers_by_station_number()