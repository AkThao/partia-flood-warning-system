from floodsystem.stationdata import build_station_list
from floodsystem.geo import rivers_with_station, stations_by_river

def run():
	"""Requirements for Task 1D"""
	# Build list of stations.
	stations = build_station_list()
	
	print('\n-- Part 1 --')
	rivers = rivers_with_station(stations)
	print('\nNumber of rivers with at least one monitoring station: ' + str(len(rivers)))
	print('\nFirst 10 alphabetically: ' + str(sorted(rivers)[:10]))
	
	print('\n-- Part 2 --')
	river_stations = stations_by_river(stations)
	print('\nStations on River Aire: ')
	print(sorted([station.name for station in river_stations['River Aire']]))
	print('\nStations on River Cam: ')
	print(sorted([station.name for station in river_stations['River Cam']]))
	print('\nStations on River Thames: ')
	print(sorted([station.name for station in river_stations['River Thames']]))

if __name__ == "__main__":
    print("*** Task 1D: CUED Part IA Flood Warning System ***")
    run()