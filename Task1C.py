from floodsystem.stationdata import build_station_list
from floodsystem.geo import stations_within_radius

def run():
    """Requirements for Task 1C"""
    # Build list of stations.
    stations = build_station_list()

    # Call stations_within_radius using the list of stations from above.
    stations_within_radius_list = stations_within_radius(stations, (52.2053, 0.1218), 10)
    print(sorted(stations_within_radius_list))

if __name__ == "__main__":
    print("*** Task 1C: CUED Part IA Flood Warning System ***")
    run()