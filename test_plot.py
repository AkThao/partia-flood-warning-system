from floodsystem.plot import plot_water_levels, plot_water_level_with_fit
from floodsystem.stationdata import build_station_list, update_water_levels
from floodsystem.datafetcher import fetch_measure_levels
from floodsystem.flood import stations_highest_rel_level
import datetime

def test_plot_water_level_with_fit():
    
    stations = build_station_list()
    
    stations = update_water_levels(stations)
    
    stations_highest = stations_highest_rel_level(stations, 5)
    
    dt = 2
    
    dates, levels = fetch_measure_levels(stations_highest[0][0].measure_id, datetime.timedelta(dt))
    plot_water_level_with_fit(stations_highest[0][0], dates, levels, 4)
    
def test_plot_water_levels():
    stations = build_station_list()
    
    stations = update_water_levels(stations)
    
    stations_highest = stations_highest_rel_level(stations, 5)
    
    dt = 2
    
    dates, levels = fetch_measure_levels(stations_highest[0][0].measure_id, datetime.timedelta(dt))
    plot_water_levels(stations_highest[0][0], dates, levels)

test_plot_water_level_with_fit()
test_plot_water_levels()