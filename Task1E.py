from floodsystem.stationdata import build_station_list
from floodsystem.geo import rivers_by_station_number


def run():
    """Requirements for Task 1E"""
    stations = build_station_list()
    rivers_with_most_stations = rivers_by_station_number(stations, 9)
    print(rivers_with_most_stations)

if __name__ == "__main__":
    print("*** Task 1E: CUED Part IA Flood Warning System ***")
    run()