from floodsystem.analysis import polyfit
from floodsystem.stationdata import build_station_list
from floodsystem.datafetcher import fetch_measure_levels
from floodsystem.flood import stations_highest_rel_level
from floodsystem.stationdata import update_water_levels
import datetime

def test_polyfit():
    
    stations = build_station_list()
    
    station = update_water_levels(stations)
    
    dt = 3
    
    dates, levels = fetch_measure_levels(station[0].measure_id, datetime.timedelta(dt))
    
    assert polyfit(dates, levels, 4)
    
    assert type(polyfit(dates, levels, 4)) == tuple

test_polyfit()