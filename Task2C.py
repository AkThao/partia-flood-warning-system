from floodsystem.stationdata import build_station_list, update_water_levels
from floodsystem.flood import stations_level_over_threshold, stations_highest_rel_level


def run():
	# Build list of stations
	stations = build_station_list()
	
	# Update latest level data for all stations
	update_water_levels(stations)
	
	# Print station and latest level for first 5 stations in list
	names = [
		'Bourton Dickler', 'Surfleet Sluice', 'Gaw Bridge', 'Hemingford',
		'Swindon'
	]
	
	over_threshold = stations_highest_rel_level(stations, 10)
	for station in over_threshold:
		print(station[0].name + ' ' + str(station[1]))
	
    # Alternative implementation
    # for station in [s for s in stations if s.name in names]:
    #     print("Station name and current level: {}, {}".format(station.name,
    #                                                           station.latest_level))


if __name__ == "__main__":
	print("*** Task 2C: CUED Part IA Flood Warning System ***")
	run()
	