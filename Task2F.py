import datetime
from floodsystem.stationdata import build_station_list
from floodsystem.datafetcher import fetch_measure_levels
from floodsystem.plot import plot_water_level_with_fit
from floodsystem.stationdata import update_water_levels
from floodsystem.flood import stations_highest_rel_level

def run():

    stations = build_station_list()
    
    stations = update_water_levels(stations)
    
    stations_highest = stations_highest_rel_level(stations, 5)
    
    dt = 2
    
    for station in stations_highest:
        
        dates, levels = fetch_measure_levels(station[0].measure_id, datetime.timedelta(dt))
        
        if len(dates) == 0:
            print("Not enough data available for {}".format(station[0].name))
        else:
            plot_water_level_with_fit(station[0], dates, levels, 4)
        

if __name__ == "__main__":
    print("*** Task 2F: CUED Part IA Flood Warning System ***")

    run()