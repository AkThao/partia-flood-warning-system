# Copyright (C) 2018 Garth N. Wells
#
# SPDX-License-Identifier: MIT

from floodsystem.stationdata import build_station_list


def run():
    """Requirements for Task 1A"""

    # Build list of stations
    stations = build_station_list()

    # Print number of stations
    print("Number of stations: {}".format(len(stations)))

    # Display data from 3 stations:
    for station in stations:
        if station.name in ['Bourton Dickler', 'Surfleet Sluice', 'Gaw Bridge']:
            print(station)


if __name__ == "__main__": # In other words, if this file is being run as a script and not as a module, then run the above function.
        # If this file were to be imported into another file as a module, anything after this if statement would not work, because __name__ is not equal to "__main__" when this file is a module.
        print("*** Task 1A: CUED Part IA Flood Warning System ***")
        run()
