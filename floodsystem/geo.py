# Copyright (C) 2018 Garth N. Wells
#
# SPDX-License-Identifier: MIT
"""This module contains a collection of functions related to
geographical data.

"""

from .utils import sorted_by_key  # noqa
from .haversine import haversine
from math import sin, cos, asin, sqrt, radians

def stations_within_radius(stations, centre, r):
	"""This function produces a list of stations within a certain radius of a particular coordinate.

		Parameters:
			stations – list of objects from the class MonitoringStation
			centre – coordinates of interest, in the form of a tuple (lat, long)
			r – radius (in km) from the centre
		
		Returns:
			stations_within_radius_list – a list of tuples of the form (station name, distance from centre (rounded to 3 d.p))
	
	"""
	stations_within_radius_list = []
	for station in stations:
		distance = haversine(centre, station.coord)
		if distance <= r:
			stations_within_radius_list.append((station.name, round(distance, 3)))

	return stations_within_radius_list

def stations_by_distance(stations, p):
	"""This function produces a list of stations and their distances from a certain coordinate.

		Parameters:
			stations – list of objects from the class MonitoringStation
			p – coordinates of interest, in the form of a tuple (lat, long)
		
		Returns:
			distances – a list of tuples of the form (station, distance from centre p)
	
	"""
	distances = []
	for station in stations:
		distances.append( (station, dist(station.coord, p)) )
	distances.sort(key=lambda x: x[1])
	
	return distances
	
def dist(s, p):
	"""Uses the haversine function to calculate the distance over the Earth between two points, s and p"""
	r = 6371
	sR = (radians(s[0]), radians(s[1]))
	pR = (radians(p[0]), radians(p[1]))
	return 2*r * asin( sqrt( sin( (sR[0]-pR[0])/2 )**2 + cos(sR[0])*cos(pR[0])*sin( (pR[1]-sR[1])/2 )**2))

def rivers_with_station(stations):
	"""This function produces a list of rivers that have at least one monitoring station.

		Parameters:
			stations – list of objects from the class MonitoringStation
		
		Returns:
			rivers – a list of river names, taken as attributes from each station object
	
	"""
	rivers = []
	for station in stations:
		#if station.river[:6] == 'River ':
		rivers.append(station.river)
	rivers = set(rivers)	# Only keep unique rivers
	return rivers
	
def stations_by_river(stations):
	"""This function produces a dictionary with key : value pairs of the form 'river name' : 'list of stations by that river'.

		Parameters:
			stations – list of objects from the class MonitoringStation
		
		Returns:
			rivers – a dictionary of river names with their corresponding station objects as values
	
	"""
	#rivers = dict.fromkeys(rivers_with_station(stations), [])
	keys = rivers_with_station(stations)
	rivers = {key: [] for key in rivers_with_station(stations)}
	
	for station in stations:
		if station.river in rivers:
			rivers[station.river].append(station)
	return rivers
	
def rivers_by_station_number(stations, N):
	"""This function produces a list of n rivers that have the highest number of monitoring stations.

		Parameters:
			stations – list of objects from the class MonitoringStation
			N – number of rivers to be returned
		
		Returns:
			n_rivers_list – a list of tuples of the form (river name, number of stations by that river)
	"""
	dict_river_number = {} # Creates an empty dictionary that will eventually hold key : value pairs in the format rivername : number of stations.
	for station in stations:
		dict_river_number[station.river] = 0 # Add a key : value pair to the dict for each station, of the form rivername : 0. This dict will now hold all the river names without duplicates.
	for station in stations:
		dict_river_number[station.river] += 1 # Add 1 to each rivername key for every station at that river. This dict will now hold all the river names with the number of stations of each.
	sorted_list_of_rivers = sorted(dict_river_number.items(), reverse = True, key = lambda x: x[1])
    # The line above does a few things:
    # The sorted function takes each key : value pair in the dict.
    # The lambda tells it to look at the second elements of each pair (which will be the value, since we want to order by number of stations).
    # Setting reverse to True tells sorted() to sort in descending order.
    # The sorted function returns a list of tuples. Each tuple represents a key : value pair from the dict and the list is ordered by tuple[1] (number of stations).

	n = N
	n_rivers_list = [] # This is the final list will contain the n rivers with the largest numbers of stations.
	for i in range(n):
	    n_rivers_list.append(sorted_list_of_rivers[i]) # Add the top n rivers to n_rivers_list.

	i = n
	if len(sorted_list_of_rivers) > n:
	    while (i >= n):
	        if sorted_list_of_rivers[i][1] == n_rivers_list[n - 1][1]: # Check if further rivers have the same number of stations as the nth river and add them if necessary.
	            n_rivers_list.append(sorted_list_of_rivers[i])
	        else:
	            break
	        i += 1

	return n_rivers_list