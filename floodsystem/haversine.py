from math import radians, cos, sin, asin, sqrt

def haversine(coord1, coord2): # Expecting two tuples, each containing a latitude and longitude for a location.
    """This function is an implementation from https://stackoverflow.com/questions/4913349/haversine-formula-in-python-bearing-and-distance-between-two-gps-points of the haversine function.

        Parameters:
        coord1, coord2 – two tuples, each containing a latitude and longitude for a location

        Returns:
        final_distance – the distance over the Earth between points coord1 and coord2
    """
    lat1 = radians(coord1[0])
    lon1 = radians(coord1[1])
    lat2 = radians(coord2[0])
    lon2 = radians(coord2[1]) # Convert all coordinates from degrees to radians.

    dlon = lon2 - lon1
    dlat = lat2 - lat1 # Differences in latitude and longitude.

    a = sin(dlat/2)**2 + cos(lat1) * cos(lat2) * sin(dlon/2)**2
    c = 2 * asin(sqrt(a)) 
    r = 6371 # Radius of earth in kilometers.
    final_distance = c * r
    return final_distance