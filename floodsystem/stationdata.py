# Copyright (C) 2018 Garth N. Wells
#
# SPDX-License-Identifier: MIT
"""This module provides interface for extracting statiob data from
JSON objects fetched from the Internet and

"""

from . import datafetcher
from .station import MonitoringStation


def build_station_list(use_cache=True):
    """Build and return a list of all river level monitoring stations
    based on data fetched from the Environment agency. Each station is
    represented as a MonitoringStation object.

    The available data for some station is incomplete or not
    available.

    """

    # Fetch station data
    data = datafetcher.fetch_station_data(use_cache)

    # Build list of MonitoringStation objects
    stations = []
    for e in data["items"]: # data is a GIGANTIC dict, "items" is a key in the data dict, the value of "items" is a list. Therefore, e is a list element, these list elements are also dicts.
        # Each e dict represents a station, with a whole load of key : value pairs representing data about the station.
        # Since each e is a station, this for loop is simply trying to gather particular information about each station and make an object for each station.
        # Not every e dict (station) has all the required information, hence, the try and except statements filter out some stations.

        # Extract town string (not always available)
        town = None
        if 'town' in e: # If the key named 'town' is present in the e dict....
            town = e['town'] # .... then town will be equal to the value of the 'town' key.

        # Extract river name (not always available)
        river = None
        if 'riverName' in e:
            river = e['riverName']

        # Attempt to extract typical range (low, high)
        try:
            typical_range = (float(e['stageScale']['typicalRangeLow']),
                             float(e['stageScale']['typicalRangeHigh']))
                             # 'stageScale' is another key in the e dict, however, its value is a dict this time.
                             # So a second index is needed. This time, the keys are 'typicalRangeLow/High' and the values are floats.
                             # So much nesting going on.
        except Exception:
            typical_range = None

        try:
            # Create mesure station object if all required data is
            # available, and add to list
            s = MonitoringStation(
                station_id=e['@id'],
                measure_id=e['measures'][-1]['@id'],
                label=e['label'],
                coord=(float(e['lat']), float(e['long'])),
                typical_range=typical_range,
                river=river,
                town=town)
                # These attributes are initialised by going through the e dict and picking out values of certain keys, such as '@id' and 'label'.
            stations.append(s)
        except Exception:
            # Not all required data on the station was available, so
            # skip over
            pass

    return stations
    # None of this is really complicated, one just has to understand the format of the data, then all the indexing will make sense.


def update_water_levels(stations):
    """Attach level data contained in measure_data to stations"""

    # Fetch level data
    measure_data = datafetcher.fetch_latest_water_level_data()

    # Build map from measure id to latest reading (value)
    measure_id_to_value = dict()
    for measure in measure_data['items']:
        if 'latestReading' in measure:
            latest_reading = measure['latestReading']
            measure_id = latest_reading['measure']
            measure_id_to_value[measure_id] = latest_reading['value']
            # A whole bunch more dictionary and list nesting.

    # Attach latest reading to station objects
    for station in stations:

        # Reset latestlevel
        station.latest_level = None

        # Attach new level data (if available)
        if station.measure_id in measure_id_to_value:
            if isinstance(measure_id_to_value[station.measure_id], float):
                station.latest_level = measure_id_to_value[station.measure_id]
    
    return stations
