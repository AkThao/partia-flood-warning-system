import matplotlib
import numpy as np
import datetime
import matplotlib.dates


def polyfit(date, levels, p):
    """Takes a list of list of dates and corresponding water levels and a integer p and returns the a list of the
    least square polynomial fit using the function polyfit and the date time shift."""
    
    date = matplotlib.dates.date2num(date)
    
    if len(date) == 0 or len(levels) == 0:
        
        poly = 0
        date = 0
    
    else:
        
        p_coeff = np.polyfit((date - date[0]), levels, p)

        #convert into usable polynomial function
        poly = np.poly1d(p_coeff)
    
    return (poly, date[0])