# Copyright (C) 2018 Garth N. Wells
#
# SPDX-License-Identifier: MIT
"""This module provides a model for a monitoring station, and tools
for manipulating/modifying station data

"""


class MonitoringStation:
	"""This class represents a river level monitoring station"""

	def __init__(self, station_id, measure_id, label, coord, typical_range,
				river, town):
		
		self.station_id = station_id
		self.measure_id = measure_id
		
		# Handle case of erroneous data where data system returns
		# '[label, label]' rather than 'label'
		self.name = label
		if isinstance(label, list):
			self.name = label[0]
		
		self.coord = coord
		self.typical_range = typical_range
		self.river = river
		self.town = town
		
		self.latest_level = None
		
	def relative_water_level(self):
		try:
			return (self.latest_level - self.typical_range[0]) / (self.typical_range[1] - self.typical_range[0])
		except:
			return None
	
	def typical_range_consistent(self):
		"""This method checks for inconsistent typical range data. Either the data is non-existent or the high level is lower than the low level.
		
			Returns:
			True – if the typical range data is consistent
			False – if the typical range data is consistent
		"""
		if self.typical_range == None:
			return False
		elif self.typical_range[0] > self.typical_range[1]: # If low is greater than high.
			return False
		else:
			return True
		
	def __repr__(self):
		d = "Station name:     {}\n".format(self.name)
		d += "   id:            {}\n".format(self.station_id)
		d += "   measure id:    {}\n".format(self.measure_id)
		d += "   coordinate:    {}\n".format(self.coord)
		d += "   town:          {}\n".format(self.town)
		d += "   river:         {}\n".format(self.river)
		d += "   typical range: {}".format(self.typical_range)
		return d

def inconsistent_typical_range_stations(stations):
	"""This function uses the typical_range_consistent method from the MonitoringStation class to determine which stations have inconsistent typical range data.
	
		Parameters:
		stations – list of objects from the class MonitoringStation
	
		Returns:
		inconsistent_stations – a list of station objects with inconsistent typical range data
	"""
	inconsistent_stations = []
	for station in stations:
		if station.typical_range_consistent() == False:
			inconsistent_stations.append(station)
	return inconsistent_stations