
from .station import MonitoringStation

def stations_level_over_threshold(stations, tol):
	'''takes a list of stations and returns a list of stations that
	have a relative water level above a given tolerance'''
	stations_list = []
	for station in stations:
		level = station.relative_water_level()
		try:
			if level > tol:
				stations_list.append( (station, level) )
		except:
			pass
	stations_list.sort(key=lambda x: x[1], reverse=True)
	return stations_list
		
def stations_highest_rel_level(stations, N):
	'''returns a list of N stations with the highest relative water level'''
	return stations_level_over_threshold(stations, 0)[:N]