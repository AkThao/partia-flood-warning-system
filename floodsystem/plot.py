import numpy as np
import matplotlib.pyplot as plt
import matplotlib
from .analysis import polyfit
from datetime import datetime, timedelta

def plot_water_levels(station, dates, levels):
    """Takes a station and list of dates and corresponding water levels for that station and date and produces a
    graph showing the water level at given date, the normal high - low range of the water level."""
    
    if len(dates) == 0 or len(levels) == 0:
        print("Not enough data available for {}".format(station.name))
    
    else:
        plt.plot(dates, levels, '.')
    
        x = np.ones(len(dates))
        y = (station.typical_range[0])*x
        z = (station.typical_range[1])*x

        plt.plot(dates, y, 'r-')
        plt.plot(dates, z, 'r-')
    
        plt.title("{}'s Water Level Data".format(station.name))
        plt.xlabel("Date")
        plt.ylabel("Water level /m")
        plt.xticks(rotation = 45)
        #plt.xlim(dates[0], dates[-1])
    
        plt.tight_layout()
        plt.show()

def plot_water_level_with_fit(station, dates, levels, p):
    """Takes a station and list of dates and corresponding water levels for that station and date and produces a
    graph showing the water level at given date, the normal high - low range of the water level and the average water
    level with time."""
    
    
    if len(dates) == 0 or len(levels) == 0:
        print("not enough data available for {}".format(station.name))
        
    else:
        
        poly, d0 = polyfit(dates, levels, p)
    
        dates = matplotlib.dates.date2num(dates)
    
        plt.plot(dates, levels, '.')
    
        x = np.ones(len(dates))
        y = (station.typical_range[0])*x
        z = (station.typical_range[1])*x
    
        plt.plot(dates, poly(dates - d0))
        plt.plot(dates, y, 'r-')
        plt.plot(dates, z, 'r-')
    
        plt.title("{}'s Water Level Data".format(station.name))
        plt.xlabel("Time /days")
        plt.ylabel("Water level /m")
        #plt.xlim(dates[0], dates[-1])
    
        plt.tight_layout()
        plt.show()