from floodsystem.stationdata import build_station_list
import floodsystem.geo as geo


def run():
	"""Requirements for Task 1B"""

    # Build list of stations
	stations = build_station_list()

	distances = geo.stations_by_distance(stations, (52.2053, 0.1218))
	
	print('\nClosest 10 stations: ')
	print([(distances[i][0].name, distances[i][0].town, distances[i][1]) for i in range(10)])
	
	print('\nFurthest 10 stations: ')
	print([(distances[i][0].name, distances[i][0].town, distances[i][1]) for i in range(len(distances)-10, len(distances))])



if __name__ == "__main__":
        print("*** Task 1B: CUED Part IA Flood Warning System ***")
        run()
