from floodsystem.flood import stations_level_over_threshold, stations_highest_rel_level
from floodsystem.stationdata import build_station_list, update_water_levels

def test_stations_over_threshold():
    
    stations = update_water_levels(build_station_list())
    
    tol = 500
    s = stations_level_over_threshold(stations, tol)
    
    assert type(s) == list
    assert s == []

    tol = 0
    
    s = stations_level_over_threshold(stations, tol)
    
    assert len(s) >= 0

    assert s[0][1] > s[1][1]

    assert s[1][1] > s[2][1]

    assert s[-1][1] < s[-2][1]

def test_stations_highest_rel_level():
    
    stations = update_water_levels(build_station_list())
    
    highest_stations = stations_highest_rel_level(stations, 10)
    
    assert len(highest_stations) ==  10

    assert highest_stations[0][1] > highest_stations[1][1]

    assert highest_stations[1][1] > highest_stations[2][1]

    assert highest_stations[-1][1] < highest_stations[-2][1]

test_stations_highest_rel_level()
test_stations_over_threshold()