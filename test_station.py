# Copyright (C) 2018 Garth N. Wells
#
# SPDX-License-Identifier: MIT
"""Unit test for the station module"""

from floodsystem.station import MonitoringStation, inconsistent_typical_range_stations
from floodsystem.stationdata import build_station_list, update_water_levels


def test_create_monitoring_station():

    # Create a station
    s_id = "test-s-id"
    m_id = "test-m-id"
    label = "some station"
    coord = (-2.0, 4.0)
    trange = (-2.3, 3.4445)
    river = "River X"
    town = "My Town"
    s = MonitoringStation(s_id, m_id, label, coord, trange, river, town)

    assert s.station_id == s_id
    assert s.measure_id == m_id
    assert s.name == label
    assert s.coord == coord
    assert s.typical_range == trange
    assert s.river == river
    assert s.town == town

test_create_monitoring_station()

def test_inconsistent_typical_range_stations():
    # Create stations with inconsistent typical range data
    s_id = "id1"
    m_id = "m1"
    label = "station1"
    coord = (-1.0, 1.0)
    trange = None
    river = "river1"
    town = "town1"
    s1 = MonitoringStation(s_id, m_id, label, coord, trange, river, town)

    s_id = "id2"
    m_id = "m2"
    label = "station2"
    coord = (1.0, -1.0)
    trange = (5.0, -5.0)
    river = "river2"
    town = "town2"
    s2 = MonitoringStation(s_id, m_id, label, coord, trange, river, town)

    stations = [s1, s2]
    inconsistent_stations = inconsistent_typical_range_stations(stations)
    assert inconsistent_stations == [s1, s2]

test_inconsistent_typical_range_stations()

def test_relative_water_level():
    
    s_id = "id1"
    m_id = "m1"
    label = "station1"
    coord = (-2.0, 4.0)
    trange = (0, 1)
    river = "river1"
    town = "town1"
    s = MonitoringStation(s_id, m_id, label, coord, trange, river, town)
    s.latest_level = None
    
    assert MonitoringStation.relative_water_level(s) == None

    stations = update_water_levels(build_station_list())
    
    stations = stations[1]

    assert type(MonitoringStation.relative_water_level(stations)) == float

test_relative_water_level()