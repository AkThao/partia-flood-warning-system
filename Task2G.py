import datetime
from floodsystem.stationdata import build_station_list
from floodsystem.datafetcher import fetch_measure_levels
from floodsystem.stationdata import update_water_levels
from floodsystem.flood import stations_highest_rel_level
from floodsystem.plot import plot_water_level_with_fit
from floodsystem.analysis import polyfit
from floodsystem.flood import stations_level_over_threshold
import matplotlib

tol = 0.5
highestRisk = True

def dateConvert(n):
	return matplotlib.dates.date2num(n)

def run():
	
	stations = build_station_list()
	
	stations = update_water_levels(stations)
	
	if highestRisk:
		stations = [v[0] for v in stations_level_over_threshold(stations, 0.6)]
	
	dt = 2
	
	severe = []
	high = []
	moderate = []
	low = []
	
	for station in stations:
		
		dates, levels = fetch_measure_levels(station.measure_id, datetime.timedelta(dt))
		try:
			poly, d0 = polyfit(dates, levels, 4)
			
			risks = []
			
			limit = station.typical_range[1]
			latest_date = dateConvert(dates[0]) - d0
			penultimate_date = dateConvert(dates[1]) - d0

			rate = (poly(latest_date) - poly(penultimate_date)) / (latest_date - penultimate_date)
			if rate > tol:
				risks.append('increasingFast')
			elif rate > 0:
				risks.append('increasingSlow')
			if levels[0] > limit:
				risks.append('aboveTypical')
				if levels[-1] < limit:
					risks.append('increasedAboveTypical')
					
				
			if risks:
				if 'increasingSlow' not in risks:
					print('\n' + station.name)
					if 'increasingFast' in risks and 'aboveTypical' in risks:
						severe.append(station)
						print('Severe risk: water level is {0:.2f} m above the typical range and increasing rapidly ({0:.4g}).'.format(levels[0] - limit, rate))
						print('Displaying graph...')
						plot_water_level_with_fit(station, dates, levels, 4)
					elif 'increasedAboveTypical' in risks:
						high.append(station)
						print('High risk: water level increased to {0:.2f} m above typical'.format(levels[0] - limit))
					elif 'aboveTypical' in risks:
						moderate.append(station)
						print('Moderate risk: water level is {0:.2f} m above typical'.format(levels[0] - limit))
					elif 'increasingFast' in risks:
						moderate.append(station)
						print('Moderate risk: water level is increasing rapidly ({0:.4g}).'.format(rate))
					#plot_water_level_with_fit(station, dates, levels, 4)
				else:
					low.append(station)

					
		except TypeError:
			#print('\n(Bad data for {}.)'.format(station.name))
			pass

if __name__ == "__main__":
	print("*** Task 2G: CUED Part IA Flood Warning System ***")
	
	run()
	