from floodsystem.stationdata import build_station_list
from floodsystem.station import inconsistent_typical_range_stations

def run():
    """Requirements for Task 1F"""
    stations = build_station_list()
    inconsistent_station_names = []
    inconsistent_stations = inconsistent_typical_range_stations(stations)
    for station in inconsistent_stations:
        inconsistent_station_names.append(station.name)
    print(sorted(inconsistent_station_names))

if __name__ == "__main__":
    print("*** Task 1F: CUED Part IA Flood Warning System ***")
    run()